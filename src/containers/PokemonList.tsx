import React from "react";
import { useQuery } from '@apollo/react-hooks';
import List from "../components/List"
import gql from 'graphql-tag';


const ALL_POKEMON = gql`
{
  pokemons( first:10) {
    id 
    number 
    name
    classification 
    types 
    maxCP 
    evolutions {
      name
      image
    }
    maxHP 
    image
    weight {
      minimum
      maximum
    }
    height {
      minimum
      maximum
    }
  }
}
`;

const CenterInfo = {
  width:'100vw',
  height:'100vh',
  display:'flex',
  justifyContent:'center',
  alignItems:'center'

}

const PokemonList = (props: any) => {
  const { loading, error, data } = useQuery(ALL_POKEMON);
  if (loading) return <div style={CenterInfo}>Loading...</div>;
  if (error) return <div style={CenterInfo}>Error :(</div>;
  const  {pokemons} = data
  return (
    <>
      <List pokemons={pokemons} />
    </>
  );
}

export default PokemonList