import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import PokemonList from './containers/PokemonList'
const client = new ApolloClient({
  uri: 'https://graphql-pokemon.now.sh/',
});

const App = () => (
  <ApolloProvider client={client}>
   <PokemonList/>
  </ApolloProvider>
);


export default App;
