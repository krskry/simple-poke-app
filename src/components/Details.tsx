import React from "react";
import Pokemon from "../models/Pokemon";
const ListItemStyle = {
  height: 500,
  width: "auto"
};

const ReturnBasicRow = (props:any) => {
  return (
    <p>
      <strong>{props.description}</strong>
      <i> {props.value}</i>
    </p>
  );
};

const CardItem = (props:any) => {
  const {
    weight,
    height,
    classification,
    maxCP,
    maxHP,
    types,
    name,
    image,
    evolutions
  } = props.pokemon;

  return (
    <div style={ListItemStyle}>
      <img src={image} alt={`pokemon ${name}`}/>
      <ReturnBasicRow description={"Pokemon name:"} value={name}/>
      <ReturnBasicRow description={"Pokemon classificcation:"} value={classification}/>
      <ReturnBasicRow description={"Pokemon min height:"} value={height.minimum}/>
      <ReturnBasicRow description={"Pokemon max height:"} value={height.maximum}/>
      <ReturnBasicRow description={"Pokemon min weight:"} value={weight.maximum}/>
      <ReturnBasicRow description={"Pokemon max weight:"} value={weight.maximum}/>
      <ReturnBasicRow description={"Pokemon maxCP:"} value={maxCP}/>
      <ReturnBasicRow description={"Pokemon maxHP:"} value={maxHP}/>
      <p>
        <strong>Pokemon types:</strong>{" "}
        {types.map((item: String, index: Number) => (
          <i key={`type-${index}`} style={{ display: "inline" }}>
            {item}
            {index !== types.length - 1 && ", "}
          </i>
        ))}
      </p>
      <p>
        <strong>Further evolutions:</strong>{" "}
        {evolutions
          ? evolutions.map((evolution: Pokemon, index: Number) => (
              <i key={`evolution-${index}`} style={{ display: "inline" }}>
                {evolution.name} {index !== evolutions.length - 1 && ", "}
              </i>
            ))
          : "No more evolutions"}
      </p>
    </div>
  );
};

export default CardItem;
