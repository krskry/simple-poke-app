import React from "react";
import Pokemon from "../models/Pokemon";

const returnListItemStyle = (isActive: boolean) => {
  return {
    height: 40,
    cursor: "pointer",
    margin: "12px 0",
    padding: 12,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: isActive ? "2px solid pink" : "2px solid grey"
  };
};

const Avatar = { height: 40, width: 40, marginRight: 12 };

const ListItem = (props: any) => {
  const { id, name, image, isActive } = props;

  return (
    <div
      onClick={() => props.handleSelect(id, Pokemon)}
      style={returnListItemStyle(isActive)}
    >
      <img src={image} alt={`${name}-pokemon`} style={Avatar} />
     
      <i> {name}</i>
    </div>
  );
};

export default ListItem;
