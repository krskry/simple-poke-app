import React, { useState } from "react";
import Pokemon from "../models/Pokemon";
import ListItem from "./ListItem";
import Details from "./Details";

const Column = {
  margin: "24px 12px",
  minWidth: 360
};
const Row = {
  display: "flex",
  justifyContent: "center"
};

const ChoosePokemonText = {
  fontSize: 48,
  height:'100%',
  padding:'auto'
}

const returnChooseText = () => {
  return <div style={ChoosePokemonText}>Choose Pokemon</div>;
};

const List = (props: any) => {
  const [current, setCurrent] = useState({});
  const [activeId, setActiveId] = useState(0);

  const handleSelect = (id: any, pokemon: Pokemon) => {
    setActiveId(id);
    setCurrent(pokemon);
  };

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>..::SIMPLE POKEapp::..</h1>
      <div style={Row}>
        <div style={Column}>
          {props.pokemons.map((pokemon: Pokemon) => (
            <div key={pokemon.id}>
              <ListItem
                handleSelect={() => handleSelect(pokemon.id, pokemon)}
                id={pokemon.id}
                name={pokemon.name}
                image={pokemon.image}
                isActive={pokemon.id === activeId}
              />
            </div>
          ))}
        </div>
        <div style={Column}>
          {current.hasOwnProperty("id") ? (
            <Details pokemon={current} />
          ) : (
            returnChooseText()
          )}
        </div>
      </div>
    </div>
  );
};

export default List;
