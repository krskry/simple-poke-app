import PokemonDimension from "./PokemonDimension";

export default class Pokemon {
  id: number;
  number: string;
  name: string;
  weight: PokemonDimension;
  height: PokemonDimension;
  classification: string;
  types: [string] ;
  maxCP: number;
  evolutions: [Pokemon];
  maxHP: number;
  image: string;
  constructor(
    id: number,
    number: string,
    name: string,
    weight: PokemonDimension,
    height: PokemonDimension,
    classification: string,
    types: [string],
    evolutions: [Pokemon],
    maxCP: number,
    image: string
  ) {
    this.id = id;
    this.number = number;
    this.name = name;
    this.weight = weight;
    this.height = height;
    this.classification = classification;
    this.types = types;
    this.maxCP = maxCP;
    this.evolutions = evolutions;
    this.maxHP = maxCP;
    this.image = image;
  }
}
