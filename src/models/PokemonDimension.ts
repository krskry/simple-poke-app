export default class PokemonDimension {
    minimum: String;
    maximum: String;
    constructor(minimum: String,maximum:String) {
        this.minimum = minimum;
        this.maximum = maximum;
    }
}